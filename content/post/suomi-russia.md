+++
draft = false
date = "2016-10-06T06:43:12+03:00"
title = "suomi russia"
image = "suomi-russian.jpg"

+++

Этот матч пройдет в Торонто на льду «Эйр Канада-центра» и начнется в 22:00 по московскому времени. Sovsport.ru проведет текстовую трансляцию матча.

Напомним, что после двух туров в группе В лидировали шведы, у которых было четыре очка. По два очка было в активе россиян и команды Северной Америки, а финны очков не имели.

Ранее на старте Кубка мира россияне уступили шведам (1:2) и одолели команду Северной Америки, составленную из игроков не старше 23 лет (4:3), а финны уступили Северной Америке (1:4) и шведам (0:2).
